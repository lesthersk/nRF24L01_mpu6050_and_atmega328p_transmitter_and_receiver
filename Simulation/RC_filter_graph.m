clear;
clc;

filename = 'RC_filter.DAT'
output_voltage = readtable(filename);%, 'ReadVariableNames',false);

plot(output_voltage.TIME,output_voltage.output_voltage, 'g',output_voltage.TIME,output_voltage.Reference, 'blue')
xlabel('TIME') % x-axis label
ylabel('OUTPUT VOLTAGE vs REFERENCE VOLTAGE') % y-axis label
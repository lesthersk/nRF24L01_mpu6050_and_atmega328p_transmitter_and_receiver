
clc; clear;
f=10e3; %frequency of sine wave in Hz
overSampRate=10; %oversampling rate
fs=overSampRate*f; %sampling frequency
duty_cycle=20; % Square wave with 50% Duty cycle (default)
nCyl = 5; %to generate five cycles of sine wave
amp = 5;
 
t=0:1/fs:nCyl*1/f; %time base
 
x=amp*square(2*pi*f*t,duty_cycle); %generating the square wave

figure;

subplot(2,1,1);
plot(t,x);
title(['Sine Wave f=', num2str(f), 'Hz']);
xlabel('Time(s)');
ylabel('Amplitude');

L=length(x);	 	 
NFFT=1024;	 	 
X=fft(x,NFFT);	 	 
Px=X.*conj(X)/(NFFT*L); %Power of each freq components	 	 
fVals=fs*(0:NFFT/2-1)/NFFT;	 	 

subplot(2,1,2);
plot(fVals,Px(1:NFFT/2),'b','LineSmoothing','on','LineWidth',1);
xlim([0 10e3])
title('One Sided Power Spectral Density');	 	 
xlabel('Frequency (Hz)')	 	 
ylabel('PSD');
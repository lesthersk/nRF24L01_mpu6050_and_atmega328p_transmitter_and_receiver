This source is for the controlling a MPU6050 and transmitting and receiving via an NRF24L01, using the Atmega328P,
and visualize it's data with the Processing software.


Copyright (c) Lesther Borge, 2018 

(https://www.linkedin.com/in/lestherborge/)

License

Released under Creative Commons Attribution-NonCommercial 4.0 International Public License.

Please refer to LICENSE file for licensing information.
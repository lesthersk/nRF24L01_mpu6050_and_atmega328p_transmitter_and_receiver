


#include <avr/io.h>
#include "pwm.h"


void setupPwm(int16_t initialDutyCycle){
    
    TCCR1A |= (1 << WGM11);                  /* Fast PWM TOP:ICR1 BOTTOM:0*/
    TCCR1B |= (1 << WGM12) | (1 << WGM13);
    TCCR1B |= (1 << CS11);                   /*Prescaler of 8*/
    ICR1    =  100;                          /*TOP value = 0.1ms*/
    TCCR1A |= (1 << COM1A1) | (1 << COM1A0); /*Using inverting mode*/
    OCR1A = initialDutyCycle;
}

void setDutyCycle(int16_t dutyCycle){
    //dutyCycle &= 0x400;
    OCR1A = dutyCycle;
    
}
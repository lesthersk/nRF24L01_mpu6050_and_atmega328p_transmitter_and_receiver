
#define F_CPU 8000000UL
#include <avr/io.h>
#include <avr/interrupt.h>

#include "peripherals.h"
#include "uart.h"
#include "mpu6050.h"
#include "nrf24l01.h"

#define PWM_OUTPUT          PINB1
#define PWM_OUTPUT_REGISTER DDRB
#define NRF24L01_INT_PIN    PIND3
#define NRF24L01_INT_REG    DDRD
#define UART_BAUD_RATE      57600 


void pinInitialization(){
    
    PWM_OUTPUT_REGISTER |= (1 << PWM_OUTPUT); /* output of the pwm signal */
    
    NRF24L01_INT_REG |= (0 << NRF24L01_INT_PIN); /* input of the NRF24L01 IRQ pin */
           
}

void uartInit(){
    
    uart_init( UART_BAUD_SELECT(UART_BAUD_RATE,F_CPU) );/* initiating uart comm */
    
}

void initNrf24l01Interrupt(){
    
    EICRA |= (1<<ISC11) | (0<<ISC10); /*  */
         
    EIMSK |= (1<<INT1); /* enable INT1 interrupt */ 
    
}

void activateInterrupts(){
    
    sei();
    
}
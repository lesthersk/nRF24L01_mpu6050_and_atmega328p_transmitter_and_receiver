
extern void pinInitialization(void);

extern void uartInit(void);

extern void initNrf24l01Interrupt(void);

extern void activateInterrupts(void);

#include <avr/io.h>

void initiateADC(){
    
    // AREF = AVcc
    ADMUX = (1<<REFS0);
    
    // ADC Enable and prescaler of 64
    // 8M/64 = 125000
    //Also an initial conversion, since its the slowest
    ADCSRA = (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1) | (0<<ADPS0) | (1<<ADSC);
    
    //Not necessary, other things to do occupy this time
    //while(ADCSRA & (1<<ADSC));
     
}

uint16_t readADC(uint8_t channel){

    // select the corresponding channel 0~7
    // ANDing with '7' will always keep the value of 'channel' between 0 and 7
    channel &= 0b00000111;  // AND operation with 7
    ADMUX = (ADMUX & 0xF8) | channel; // clears the bottom 3 bits before ORing
    
    // start single conversion
    // write '1' to ADSC
    ADCSRA |= (1<<ADSC);
    
    // wait for conversion to complete
    // ADSC becomes '0' again
    // till then, run loop continuously
    while(ADCSRA & (1<<ADSC));
    
    return (ADC);

}

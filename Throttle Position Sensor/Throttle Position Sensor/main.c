/*
 * Throttle Position Sensor.c
 *
 * Created: 7/28/2017 10:07:59 PM
 * Author : lesth
 */ 

#define F_CPU 8000000UL

#include <avr/io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>

#include "pwm.h"
#include "peripherals.h"
#include "uart.h"
#include "adc.h"
#include "nrf24l01.h"
#include "nrf24l01registers.h"
#include "mpu6050.h"

//Initial duty cycle definition
#define initialDutyCycle 0

/* definition on who is who */
#define role_tx 1
#define role_rx 0

uint8_t incomingMessage = 0;
uint8_t role = role_rx;
uint8_t i = 0;

volatile char t;
volatile long *ptr = 0;

ISR(INT1_vect){

    //uart_puts("\r\nMessage arrived\r\n");
    incomingMessage = 1;

}

int main(void){

    //Reserved for future use
    double qx;
    double qw;
    double qy;
    double qz;
    double roll;
    double pitch;
    double yaw;
    uint8_t mpu_data [NRF24L01_PAYLOAD];
    
    //sending buffer addresses
    uint8_t sendpipe = 0;
    uint8_t addrtx0[NRF24L01_ADDRSIZE] = NRF24L01_ADDRP0;
    uint8_t bufferin[NRF24L01_PAYLOAD];
    
    uartInit(); 
    
    pinInitialization(); //Initialize pins for nfr sensor and pwm output
    initNrf24l01Interrupt(); //enable interrupts for nfr
    nrf24l01_init(); //initialize nfr sensor
    activateInterrupts(); //activate global interrupts
    
    uart_puts("\fFunciona la comunicacion serial =D\r\n");
    nrf24l01_printinfo(uart_puts, uart_putc);
    uart_puts("\r\n\r\n");
           
    if (role == role_tx){
        
        uart_puts ("Initializing MPU\r\n");
        mpu6050_init();
    
        uart_puts ("Initializing DMP\r\n");
        _delay_ms(200);
         
        while (!i){

            //initialize mpu6050 dmp processor
            switch (mpu6050_dmpInitialize()){
             
                case 0:
             
                uart_puts ("DMP enabled successfully =D\r\n");
                mpu6050_dmpEnable();
                _delay_ms(10);
                i = 1;
             
                break;
             
                case 1:
             
                uart_puts ("main binary block loading failed\r\n");
                uart_puts ("Retrying\n\n");
                _delay_ms(1000);
             
                break;
             
                case 2:
             
                uart_puts ("configuration block loading failed\r\n");
                uart_puts ("Retrying\n\n");
                _delay_ms(1000);
             
                break;
            }
         
        }
        
        uart_puts ("Both things configured!!\r\n");
        _delay_ms(1000);
        
    }
              
    
    while (1){
        
        if (role == role_tx){
                         
            if(mpu6050_getQuaternionWait(&qw, &qx, &qy, &qz)) {
                uart_puts ("Getting data\r\n");
                //uart_puts ("mpu6050_getQuaternionWait if condition ");
                mpu6050_getRollPitchYaw (qw, qx, qy, qz, &roll, &pitch, &yaw);
                
                char pipebuffer[5];
                uart_puts("sending data, on pipe ");
                itoa(sendpipe, pipebuffer, 10);
                uart_puts(pipebuffer);
                uart_puts("... ");
                
                for (i = 0; i < NRF24L01_PAYLOAD/7; i++){
                    mpu_data[i]      = *(long *)(&qx) >> 8*i;
                    mpu_data[i + 4]  = *(long *)(&qw) >> 8*i;
                    mpu_data[i + 8]  = *(long *)(&qy) >> 8*i;
                    mpu_data[i + 12] = *(long *)(&qz) >> 8*i;
                    mpu_data[i + 16] = *(long *)(&roll) >> 8*i;
                    mpu_data[i + 20] = *(long *)(&pitch) >> 8*i;
                    mpu_data[i + 24] = *(long *)(&yaw) >> 8*i;
                                    
                }
                
                nrf24l01_settxaddr(addrtx0);
                uint8_t writeret = nrf24l01_write(mpu_data);
                                
                if(writeret == 1)
                    uart_puts("ok\r\n");
                else
                    uart_puts("failed\r\n");

            }
            _delay_ms(10);
                                               
        }
        
        
        if (role == role_rx){
            
            uint8_t pipe = 0;
            uint8_t counter_help = 1;
            if(incomingMessage) { //if data is ready
                
                nrf24l01_readready(&pipe);
                
                if (!pipe){
                    
                    incomingMessage = 0;          
                    //read buffer
                    nrf24l01_read(bufferin);
                    
                    
                    for(i=0; i < NRF24L01_PAYLOAD; i++)
                        uart_putc(bufferin[i]);
                        
                    uart_puts("\n");                           
                                
                    for(i=0; i<sizeof(bufferin); i++)
                        bufferin[i] = 0;
 
                }
                
            }   
          
          //uart_puts("waiting for data...\r\n");
           
        } 
        
        _delay_ms(40);           
        
    }/* while (1) end*/
     
}
